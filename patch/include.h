/* Patches */
#include "attachx.h"
#include "cfacts.h"
#include "dragcfact.h"
#include "dragmfact.h"
#include "exresize.h"
#include "maximize.h"
#include "pertag.h"
#include "systray.h"
#include "vanitygaps.h"
/* Layouts */
#include "centeredmaster.h"
#include "fibonacci.h"
#include "gapplessgrid.h"
#include "monocle.h"
#include "tile.h"
